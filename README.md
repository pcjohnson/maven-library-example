#Overview

The goal of this repository is to provide a *high quality*, *professional*  example for setting up a Continuous Integration pipeline using [BitBucket Pipelines](https://bitbucket.org/product/features/pipelines)
to build and publishes a maven artifact (Java Library) to a maven repository.

It provides two triggers:

1. On all commits to master, trigger a `mvn deploy`
2. After the first trigger has successfully run, you can manually perform a `mvn release:prepare` which will add two commits to the repository.
     * These two commits will be picked up by the first trigger and will result in deploying the new snapshot and release version to your maven repository.  

This is one of two examples that were used in the article [Continuous Integration: A Complete Guide to Using BitBucket Pipelines With Private Maven Repositories](https://www.cloudrepo.io/articles/continuous-integration-a-complete-guide-to-bitbucket-pipelines-with-maven-repositories.html)

The second example, for reading from a Maven Repository with BitBucket Pipelines can be found here: TODO: Add link. 

# Universal Example

We hope this examples provides a benefit to all [Apache Maven](https://maven.apache.org/) and BitBucket Pipeline users.


While this example uses a [CloudRepo](https://www.cloudrepo.io)  [private maven repository](https://www.cloudrepo.io/private-maven-repositories.html) to deploy artifacts,  any other maven compatible repository can be used with minor modifications.


Once this library has been published to a maven repository, the library can be added to any other Maven project.

# Getting Started

For an in-depth walk through, please read our [Continuous Integration: A Complete Guide to Using BitBucket Pipelines With Maven Repositories](https://www.cloudrepo.io/articles/continuous-integration-complete-guide-bitbucket-pipelines-maven-repositories.html) article.

If you just want to get started perform the following steps:

1. [Fork this repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) to your own BitBucket account.
3. Enable BitBucket Pipelines in your newly Forked Repository
     * Settings | Pipelines (Settings) | Enable Pipelines
     * ![Enabling BitBucket Pipelines](images/010-Enable-BitBucket-Pipelines.png)
     * **Note:** The first build trigger should not fire until a new commit has been pushed after pipelines are enabled (which we'll do in a later step).
4. Add Secure Environment Variables for Your Pipeline
     * Settings | Pipelines (Enviroment Variables) 
     * Add the Following Enviroment Variables
       * **CLOUDREPO_USERNAME**
         * This is the username for pushing and pulling artifacts from your maven repository.  This will end up in your settings.xml file.
         * **Optional:** To prevent logging of this username you can set the enviroment variable to **secret**.
       * **CLOUDREPO_PASSPHRASE**
         * This is the passphrase for pushing and pulling artifacts from your maven repository.  This will end up in your settings.xml file.
         * **RECOMMENDED:** To prevent logging of this passphrase, ensure that you set the enviroment variable to **secret**.
       * **GIT_USER_EMAIL**
         * Used only when the `mvn release:prepare` trigger is run.  This is the user email by Git used when Pipelines commits back to this repository.
         * **Note:** This does not have to correspond to an actual user's email, it will only show up in your Git commit logs as the author of any commit performed by BitBucket Pipelines.  
       * **GIT_USER_NAME**  
         * Used only when the `mvn release:prepare` trigger is run.  This is the user name used by Git when Pipelines commits back to this repository.
         * **Note:** This does not have to correspond to an actual user's name, it will only show up in your Git commit logs as the author of any commit performed by BitBucket Pipelines.
       * ![BitBucket Pipelines Environment Variables](images/020-Environment-Variables.png)
5. **(For Maven Releases Only):** Create SSH KeyPair
     * **Note:** *This step may no longer be necessary with the new [Git Push Changes](https://community.atlassian.com/t5/Bitbucket-Pipelines-articles/Pushing-back-to-your-repository/ba-p/958407) that BitBucket has implemented.*
     * We a need key pair to allow BitBucket Pipelines to `git push` back to this repo when we run are releasing an artifact via a `mvn release:perform` command.
     * Settings | Pipelines (SSH keys) | Generate Keys
     * ![BitBucket Pipeline Create SSH KeyPair](images/025-Create-Pipeline-SSH-KeyPair.png)
6. **(For Maven Releases Only):** Add Public Key to Your Account's list of SSH Keys
     * **Note:** *This step may no longer be necessary with the new [Git Push Changes](https://community.atlassian.com/t5/Bitbucket-Pipelines-articles/Pushing-back-to-your-repository/ba-p/958407) that BitBucket has implemented.*
     * Copy the **public key** generated in the previous step.
     * In your **Account Settings** (not the BitBucket Pipelines Settings), add a new SSH Key
       * Settings | Security | SSH Keys | Add Key
       * Paste the **public key** from the BitBucket Pipeline here
       * ![BitBucket Add SSH Key Pair](images/030-Create-SSH-KeyPair.png) 
7. Update the `<distributionManagement>` section of `pom.xml` to point to your Maven Repository URLs for both a Snapshot and Release Maven repository. 
     * Update the `<url>` tags with these values.
     * If you update the `<id>` tags, make sure you make corresponding changes to the `settings.template.xml` file.
     * If you don't have accessible repositories, CloudRepo offers cloud based maven repositories that are [free to try](https://www.cloudrepo.io/signup.html) for 14 days.
     * Example:
         * **Releases:** https://cloudrepo-examples.mycloudrepo.io/repositories/maven-releases
         * **Snapshots:** https://cloudrepo-examples.mycloudrepo.io/repositories/maven-snapshots
8. Commit your changes to `pom.xml` and push them to your forked repository, this should trigger a new build.
9. You can view the status of Pipelines by going to the `Pipelines` menu option in your BitBucket Repository Page.  
10. Pipelines should automatically start building any commits to the master branch.

# Validating Deployment

Validate Successful build and deployment in two places:

1. BitBucket Pipelines Status
2. Maven Repository Storage

## BitBucket Pipelines Status

You can refer to the `Pipelines` menu option for your BitBucket Repository to see the status of your builds.

After successful execution of this example, you should see the following after your first successful build:

![First BitBucket Pipeline Step Execution](images/040-Snapshot-Build-Success.png)

When you are ready to create a release version of your artifact, click the 'Run' button in the 'Create Release Version' step. 
This will kick of a `mvn release:perform` which will create two new commits on your `master` branch.  

When the 'Create Release Versions' step is done, the build results will look like the following:
 
![Second BitBucket Pipeline Step Execution](images/050-Release-Build-Success.png)

When you go back to the Pipeline Summary you will see that two new builds have been completed, one for the new release version and another for the next version of the snapshot.

![Final BitBucket Pipeline Summary](images/060-Build-Pipelines-Success.png)

If your BitBucket Pipeline history looks like this then you've completed this example successfully.


## Maven Repository Storage

Since these pipelines deployed Snapshot artifacts to a Maven Repository, log into your repositories user interface to verify the artifacts have been successfully deployed.

We'll show examples from the [CloudRepo Admin Portal](https://admin.cloudrepo.io) below:

## Snapshot Release

Navigating to the `maven-snapshots` repository in CloudRepo, we can validate that the next version of the snapshot has been release:

![CloudRepo Snapshot Release](images/070-CloudRepo-Snapshots.png)

## Release Version

Navigating to the `maven-releases` repository in CloudRepo, we can validate the `1.0.0` release version has been successfully deployed.

![CloudRepo Release Version](images/080-CloudRepo-Releases.png)


# Thank You

While we do our best to ensure a high quality example, we might have missed something.   If you hit a snag, please open an issue so that we can fix it for others who are seeking similar knowledge.

This example was brought to you by the engineering team at [CloudRepo](https://www.cloudrepo.io).  If you want to contact us directly, visit our [Contact Us Page](https://www.cloudrepo.io/contact.html)  

Please feel free to share on Social Media, Medium, or other channeles.